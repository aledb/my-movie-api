from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from config.database import engine, Base, Session
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from models.computadoras import Computadoras as ComputadorasModel
from fastapi import Depends
from middlewares.jwt_bearer import JWTBearer
from typing import List, Optional
from routers.user import user_router

app = FastAPI()
app.title = "Computadoras y Películas"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

#MENSAJE

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>BIENVENIDOS A MI TIENDA DE COMPUTADORAS</h1>')

##################################################                     COMPUTADORAS                     ##################################################
        
class Computadoras(BaseModel):
    id: Optional[int] = None
    marca: str = Field(default="Marca Computadora", min_length=5, max_length=15)
    modelo: str = Field(default="Modelo Computadora", min_length=5, max_length=15)
    color: str = Field(default="Color Computadora", min_length=5, max_length=15)
    ram: int = Field(default="RAM Computadora")
    almacenamiento: str = Field(default="Almacenamiento Computadora", min_length=5, max_length=15)

    class Config:
        json_schema_extra ={
            "example":{
                "id": 1,
                "marca": "Apple",
                "modelo": "Macbook M3 Pro",
                "color": "Space Gray",
                "ram": "42",
                "almacenamiento": "1 tb SSD"
            }
        }

computadoras = [
    {
        "id": 1,
        "marca": "Apple",
        "modelo": "Mackbook M3 Pro",
        "color":"Space Gray",
        "ram":"42",
        "almacenamiento":"1 tb SSD"
    },
    {
        "id": 2,
        "marca": "Lenovo",
        "modelo": "Thinkpad X1",
        "color":"Negro",
        "ram":"16",
        "almacenamiento":"1 tb SSD"
    },
    {
        "id": 3,
        "marca": "Asus",
        "modelo": "Vivobook",
        "color":"Gris",
        "ram":"16",
        "almacenamiento":"512 gb SSD"
    },
    {
        "id": 4,
        "marca": "Apple",
        "modelo": "Macbook Air M1",
        "color":"Space Gray",
        "ram":"16",
        "almacenamiento":"512 gb SSD"
    },
    {
        "id": 5,
        "marca": "HP",
        "modelo": "Probook",
        "color":"Gris",
        "ram":"16",
        "almacenamiento":"512 gb SSD"
    }
]

@app.get('/computadoras/', tags=['computadoras'], response_model=List[Computadoras], dependencies=[Depends(JWTBearer())])
def get_computadoras() -> List[Computadoras]:
    db = Session()
    computadoras = db.query(ComputadorasModel).all()
    return computadoras

@app.get('/computadoras/{id}', tags=['computadoras'], response_model=Computadoras, dependencies=[Depends(JWTBearer())])
def get_computadora_by_id(id: int) -> Computadoras:
    db = Session()
    computadora = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not computadora:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    return computadora

@app.post('/computadoras/', tags=['computadoras'], response_model=List[Computadoras], status_code=200, dependencies=[Depends(JWTBearer())])
def create_computadora(computadora: Computadoras) -> dict:
    db = Session()
    new_computadora = ComputadorasModel(**computadora.dict())
    db.add(new_computadora)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la computadora"})

@app.put('/computadoras/{id}', tags=['computadoras'], response_model=Computadoras, status_code=200, dependencies=[Depends(JWTBearer())])
def update_computadora(id: int, computadora: Computadoras) -> dict:
    db = Session()
    result = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    result.marca = computadora.marca
    result.modelo = computadora.modelo
    result.color = computadora.color
    result.ram = computadora.ram
    result.almacenamiento = computadora.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha actualizado la computadora"})

@app.delete('/computadoras/{id}', tags=['computadoras'], response_model=List[Computadoras], status_code=200, dependencies=[Depends(JWTBearer())])
def delete_computadora(id: int) -> dict:
    db = Session()
    result = db.query(ComputadorasModel).filter(ComputadorasModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"}) 
