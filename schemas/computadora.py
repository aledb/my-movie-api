from pydantic import BaseModel, Field
from typing import Optional

class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length=1)
    modelo: str = Field(min_length=1)
    color: str = Field(min_length=1)
    ram: str = Field(min_length=1)
    almacenamiento: str = Field(min_length=1)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "marca": "Mi Computadora",
                "modelo": "Modelo de la Computadora",
                "color": "Color de la Computadora",
                "ram": "RAM de la Computadora",
                "almacenamiento": "Almacenamiento de la Computadora"
            }
        }

